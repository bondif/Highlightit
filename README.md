# Highlightit Mobile App
Hybrid mobile app for managing school notes and modules. app created using ionic 3

# Steps to use
* clone this repo
* npm install
* change object at "Highlightit/src/app/app.firebase.config.ts" by your own firebase credentials
* Then ```ionic build android``` or ```ionic build ios```

NB# This is just an experimental project !!
